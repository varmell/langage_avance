#pragma once

class Point
{
private: 
	int x;
	int y;
public:
	Point();
	~Point();
	Point(const int x,const int y);
	Point(const Point& p);
	void afficher()const;
	void cloner(const Point &p);
	int getX();
	int getY();
};
