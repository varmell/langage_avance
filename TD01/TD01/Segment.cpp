#include "StdAfx.h"
#include "Segment.h"
#include <iostream>
#include <math.h>

using namespace std;


Segment::Segment(void):p1(0,0),p2(0,0){}
Segment::Segment(const Point& p1,const Point& p2):p1(p1),p2(p2){}
Segment::~Segment()
{
	cout<<"appel au destructeur";
};
double Segment::longueur()
{
	return sqrt(pow(double(p2.getX()-p1.getX()),2)+pow(double(p2.getY()-p1.getY()),2));
}
bool Segment::estVertical()
{
	return p1.getX()==p2.getX();
}
bool Segment::estHorizontal()
{
	return p1.getY()==p2.getY();
}
bool Segment::estSurDiagonale()
{
	return !(this->estVertical() || this->estHorizontal());
}
