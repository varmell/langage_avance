#include "StdAfx.h"
#include "CSString.h"

int CSString::nbrChaine = 0;
CSString::CSString(void)
{
	chaine= new char[1];
	chaine[0]='\0';
	nbrChaine++;
}
CSString::CSString(const char* c)
{
	chaine = new char[strlen(c)+1];
	strcpy(chaine,c);
	nbrChaine++;
}
CSString::CSString(const char c)
{
	chaine = new char[2];
	chaine[0]=c;
	chaine[1]='\0';
	nbrChaine++;
}
CSString::CSString(const CSString& copy)
{
	chaine = new char[strlen(copy.chaine)+1];
	strcpy(chaine,copy.chaine);
	nbrChaine++;
}
string CSString::getString()
{
	return chaine;
}
int CSString::nbrChaines()
{
	return nbrChaine;
}
CSString CSString::plus(const char c)const
{
	int taille = strlen(chaine);
	char* tmp = new char [taille+2];
	strcpy(tmp,chaine);
	tmp[taille]=c;
	tmp[taille + 1]='\0';
	CSString cs(tmp);
	delete []tmp;
	return cs;
}
CSString::~CSString()
{
	if(chaine)
	{
		delete []chaine;
		nbrChaine--;
	}
}

bool CSString::plusGrandQue(const CSString &s)const{
	if(strcmp(chaine,s.chaine)>0)
		return true;
	return false;
}

bool CSString::infOuEgale(const CSString &s)const{
	return !plusGrandQue(s);
}

CSString CSString::plusGrand(const CSString &s){
	if(plusGrandQue(s))
		return *this;
	return s;
}
CSString& CSString::operator=(const CSString& copy)
{
	CSString tmp(copy);
	swap(tmp.chaine,chaine);
	return *this;
}
CSString CSString::operator+(const char c)
{
	return plus(c);
}
bool CSString::operator>(const CSString &s)
{
	return plusGrandQue(s);
}
bool CSString::operator<=(const CSString &s)
{
	return !plusGrandQue(s);
}

