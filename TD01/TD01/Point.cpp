#include "StdAfx.h"
#include "Point.h"
#include <iostream>

using namespace std;

Point::Point():x(5),y(10){}
Point::Point(const int x, const int y):x(x),y(y){}
Point::Point(const Point& p):x(p.x),y(p.y){}
Point::~Point()
{
	cout<<"appel au destructeur";
};
void Point::cloner(const Point &p)
{
	x=p.x;y=p.y;
}
void Point::afficher()const
{
	cout << this->x << endl;
	cout << this->y << endl;
};
int Point::getX()
{
	return x;
}
int Point::getY()
{
	return y;
}
