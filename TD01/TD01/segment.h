#pragma once
#include "Point.h"

class Segment
{
private:
	Point p1;
	Point p2;
public:
	Segment(void);
	Segment(const Point& p1, const Point& p2);
	~Segment();
	double longueur();
	bool estVertical();
	bool estHorizontal();
	bool estSurDiagonale();
};
