#pragma once
#include <string>

using namespace std;

class CSString
{
private:
	char* chaine;
	static int nbrChaine;
public:
	CSString(const CSString& copy);
	CSString(void);
	CSString(const char* c);
	CSString(const char c);
	string getString();
	static int nbrChaines();
	virtual ~CSString();
	CSString plus(char c)const;
	bool plusGrandQue(const CSString &s)const;
	bool infOuEgale(const CSString &s)const;
	CSString plusGrand(const CSString &s);
	CSString& operator=(const CSString& copy);
	CSString operator+(const char c);
	bool operator>(const CSString &s);
	bool operator<=(const CSString &s);
};
